all: build

build:
	cabal install --only-dependencies
	cabal build

install:
	cabal install --global

clean:
	cabal clean
