Peregrine
=======

Tool for efficient verification of population protocols

Installation
-------
In order to compile and install Peregrine, go to the project's main directory and enter
```bash
    sudo make install
```

Compilation requires the Glasgow Haskell Compiler [ghc >= 7.8.1](https://www.haskell.org/ghc/)
and the build system [cabal >= 1.22](https://www.haskell.org/cabal/).

Input
------

Peregrine takes a single population protocol as JSON-encoded input.
The following example shows the input format:

```
{
    "title": "Majority Protocol",
    "states": ["A", "a", "B", "b"],
    "transitions": [
                    { "name": "t1",
                      "pre": ["A", "B"],
                      "post": ["a", "b"]
                    },
                    { "name": "t2",
                      "pre": ["B", "a"],
                      "post": ["B", "b"]
                    },
                    { "name": "t3",
                      "pre": ["A", "b"],
                      "post": ["A", "a"]
                    },
                    { "name": "t4",
                      "pre": ["a", "b"],
                      "post": ["a", "a"]
                    }
                   ],
    initialStates: ["A", "B"],
    trueStates: ["A", "a"],
    predicate: "A >= B",
    description: "This protocol computes whether there are more B-states than A-states"
}


```

Usage
-------

Peregrine is run from the command line.

If you save the protocol from above in the file *majority.pp* and enter:
```bash
    peregrine majority.pp
```

You obtain the following output:
```
Peregrine - Efficient Verification of Population Protocols

Reading "majority.pp"
Analyzing Population protocol "Majority Protocol"

Checking layered termination
Checking layered termination with at most 1 layers
Checking layered termination with at most 2 layers
layered termination satisfied

Checking strong consensus
strong consensus satisfied

All properties satisfied
```

As seen in the output, Peregrine by default checks two properties:
*Layered Termination* and *Strong Consensus*:

* Layered Termination: protocol is layered in a way that every fair run of the protocol
will eventually reach a terminal population where no more change occurs.
* Strong Consensus: every terminal population reachable from an initial population
  forms a consensus. Moreover, the boolean value of the consensus must be unique
  for a given initial population.

If both Layered Termination and Strong Consensus are satsified, then the protocol
is *well-specified*, i.e. the protocol robustly computes a predicate defined
by the mapping from input population to its unique consensus value.
Note that the converse direction need not hold: there are well-specified protocols
that violate the conjunction of Strong Consensus and Layered Termination.

Peregrine offers fine-grained control over the solution methods and the
verbosity of the output.
To see all options, enter:
```bash
    peregrine --help
```

Benchmarks
-------

In the `benchmarks` directory, scripts and tools to generate protocols
and evaluate Peregrine on these benchmarks can be found.
To run the benchmarks, Peregrine needs to be installed.
To compile the table to display the results, additionally
[Python 3](https://www.python.org/) with the [pandas](http://pandas.pydata.org/) library and
[LaTeX](https://www.latex-project.org/) are required.

In order to reproduce the results from the paper
"Towards Efficient Verification of Population Protocols",
go to the folder `benchmarks` and run
```bash
    make podc
```
The results are then compiled to the file `results-podc.pdf`.

In order to run all benchmarks, go to the folder `benchmarks` and run
```bash
    make
```
The results are then compiled to the file `results-complete.pdf`.

Authors
-------

* Philipp Meyer (<meyerphi@in.tum.de>)
* Stefan Jaax (<jaax@in.tum.de>)

References
--------
* Michael Blondin, Javier Esparza, Stefan Jaax, Philipp J. Meyer: “Towards Efficient Verification of Population Protocols”, 2017; [arXiv:1703.04367](http://arxiv.org/abs/1703.04367).

License
-------
Peregrine is licensed under the GNU GPLv3, see [LICENSE.md](LICENSE.md) for details.

Logo by [Juan Lacruz](https://commons.wikimedia.org/wiki/User:Juan_lacruz), [Peregrine Falcon La Cañada](https://commons.wikimedia.org/wiki/File:Peregrine_Falcon_La_Cañada.jpg), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode).

