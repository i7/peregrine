{
    "transitions": [
        {
            "pre": [
                "AY",
                "AT"
            ],
            "post": [
                "AY",
                "PY"
            ],
            "name": "x__AY__AT"
        },
        {
            "pre": [
                "AY",
                "AN"
            ],
            "post": [
                "AT",
                "PT"
            ],
            "name": "x__AY__AN"
        },
        {
            "pre": [
                "AT",
                "AN"
            ],
            "post": [
                "AN",
                "PN"
            ],
            "name": "x__AT__AN"
        },
        {
            "pre": [
                "AT",
                "AT"
            ],
            "post": [
                "AT",
                "PT"
            ],
            "name": "x__AT__AT"
        },
        {
            "pre": [
                "AY",
                "PN"
            ],
            "post": [
                "AY",
                "PY"
            ],
            "name": "x__AY__PN"
        },
        {
            "pre": [
                "AY",
                "PT"
            ],
            "post": [
                "AY",
                "PY"
            ],
            "name": "x__AY__PT"
        },
        {
            "pre": [
                "AN",
                "PY"
            ],
            "post": [
                "AN",
                "PN"
            ],
            "name": "x__AN__PY"
        },
        {
            "pre": [
                "AN",
                "PT"
            ],
            "post": [
                "AN",
                "PN"
            ],
            "name": "x__AN__PT"
        },
        {
            "pre": [
                "AT",
                "PN"
            ],
            "post": [
                "AT",
                "PT"
            ],
            "name": "x__AT__PN"
        },
        {
            "pre": [
                "AT",
                "PY"
            ],
            "post": [
                "AT",
                "PT"
            ],
            "name": "x__AT__PY"
        }
    ],
    "states": [
        "AY",
        "AT",
        "AN",
        "PY",
        "PT",
        "PN"
    ],
    "initialStates": [
        "AY",
        "AN"
    ],
    "predicate": "AY >= AN",
    "trueStates": [
        "AY",
        "AT",
        "PY",
        "PT"
    ],
    "title": "Fast Majority Protocol"
}
