{
    "transitions": [
        {
            "pre": [
                "_mod0",
                "_mod0"
            ],
            "post": [
                "_mod0",
                "_modpassivefalse"
            ],
            "name": "x__mod0__mod0"
        },
        {
            "pre": [
                "_mod0",
                "_mod1"
            ],
            "post": [
                "_mod1",
                "_modpassivetrue"
            ],
            "name": "x__mod0__mod1"
        },
        {
            "pre": [
                "_mod0",
                "_mod2"
            ],
            "post": [
                "_mod2",
                "_modpassivefalse"
            ],
            "name": "x__mod0__mod2"
        },
        {
            "pre": [
                "_mod0",
                "_modpassivetrue"
            ],
            "post": [
                "_mod0",
                "_modpassivefalse"
            ],
            "name": "x__mod0__modpassivetrue"
        },
        {
            "pre": [
                "_mod1",
                "_mod1"
            ],
            "post": [
                "_mod2",
                "_modpassivefalse"
            ],
            "name": "x__mod1__mod1"
        },
        {
            "pre": [
                "_mod1",
                "_mod2"
            ],
            "post": [
                "_mod0",
                "_modpassivefalse"
            ],
            "name": "x__mod1__mod2"
        },
        {
            "pre": [
                "_mod1",
                "_modpassivefalse"
            ],
            "post": [
                "_mod1",
                "_modpassivetrue"
            ],
            "name": "x__mod1__modpassivefalse"
        },
        {
            "pre": [
                "_mod2",
                "_mod2"
            ],
            "post": [
                "_mod1",
                "_modpassivetrue"
            ],
            "name": "x__mod2__mod2"
        },
        {
            "pre": [
                "_mod2",
                "_modpassivetrue"
            ],
            "post": [
                "_mod2",
                "_modpassivefalse"
            ],
            "name": "x__mod2__modpassivetrue"
        }
    ],
    "states": [
        "_mod0",
        "_mod1",
        "_mod2",
        "_modpassivetrue",
        "_modpassivefalse"
    ],
    "initialStates": [
        "_mod0",
        "_mod1",
        "_mod2"
    ],
    "predicate": "0 * _mod0 + 1 * _mod1 + 2 * _mod2 =%3 1",
    "trueStates": [
        "_mod1",
        "_modpassivetrue"
    ],
    "title": "Modulo Protocol with m = 3 and c = 1"
}
