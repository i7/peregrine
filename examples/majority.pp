{
    "transitions": [
        {
            "pre": [
                "_a",
                "_b"
            ],
            "post": [
                "_asmall",
                "_bsmall"
            ],
            "name": "x__a__b"
        },
        {
            "pre": [
                "_a",
                "_bsmall"
            ],
            "post": [
                "_a",
                "_asmall"
            ],
            "name": "x__a__bsmall"
        },
        {
            "pre": [
                "_b",
                "_asmall"
            ],
            "post": [
                "_b",
                "_bsmall"
            ],
            "name": "x__b__asmall"
        },
        {
            "pre": [
                "_asmall",
                "_bsmall"
            ],
            "post": [
                "_bsmall",
                "_bsmall"
            ],
            "name": "x__asmall__bsmall"
        }
    ],
    "states": [
        "_a",
        "_b",
        "_asmall",
        "_bsmall"
    ],
    "initialStates": [
        "_a",
        "_b"
    ],
    "predicate": "_a > _b",
    "trueStates": [
        "_a",
        "_asmall"
    ],
    "title": "Majority Protocol"
}