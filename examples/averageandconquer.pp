{
    "title": "Average and Conquer protocol",
    "states": ["G0", "B0", "G3", "B3", "G1_1", "B1_1"],
    "transitions": [
        {"name": "G0, B3 -> B0, B3", "pre": ["G0", "B3"], "post": ["B0", "B3"]},
        {"name": "G0, B1_1 -> B0, B1_1", "pre": ["G0", "B1_1"], "post": ["B0", "B1_1"]},
        {"name": "B0, G3 -> G0, G3", "pre": ["B0", "G3"], "post": ["G0", "G3"]},
        {"name": "B0, G1_1 -> G0, G1_1", "pre": ["B0", "G1_1"], "post": ["G0", "G1_1"]},
        {"name": "G3, B3 -> B1_1, G1_1", "pre": ["G3", "B3"], "post": ["B1_1", "G1_1"]},
        {"name": "G3, B1_1 -> G1_1, G1_1", "pre": ["G3", "B1_1"], "post": ["G1_1", "G1_1"]},
        {"name": "B3, G1_1 -> B1_1, B1_1", "pre": ["B3", "G1_1"], "post": ["B1_1", "B1_1"]},
        {"name": "G1_1, B1_1 -> B0, G0", "pre": ["G1_1", "B1_1"], "post": ["B0", "G0"]}
    ],
    "initialStates": ["G3", "B3"],
    "trueStates": ["G0", "G3", "G1_1"],
    "initialPredicate": "G3 != B3",
    "predicate": "G3 >= B3",
    "precondition": "G3 != B3",
    "description":
        "This protocol is a fast and exact majority protocol. It computes\n
        whether there are initially more agents voting yes (represented by\n
        state Gm) than agents voting no (represented by state Bm). The protocol\n
        does not handle ties. Parameters m and d control the running time\n
        of the protocol. Described in Dan Alistarh, Rati Gelashvili,\n
        Milan Vojnovi\u0107. Fast and Exact Majority in Population Protocols.\n
        PODC 2015."
}
