{ "title": "Broadcast Protocol",
  "states": ["_true", "_false"],
  "transitions": [{ "name": "true,false->true,true",
                    "pre": ["_true", "_false"],
                    "post": ["_true", "_true"]
                }
               ],
  "initialStates": ["_true", "_false"],
  "trueStates": ["_true"],
  "predicate": "_true >= 1"
}
