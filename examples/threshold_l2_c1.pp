{
    "transitions": [
        {
            "pre": [
                "_l_true_m2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__l_true_m2__l_true_m2"
        },
        {
            "pre": [
                "_l_true_m2",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__l_true_m2__l_true_m1"
        },
        {
            "pre": [
                "_l_true_m2",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_true_m2__l_true_0"
        },
        {
            "pre": [
                "_l_true_m2",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_true_m2__l_true_p1"
        },
        {
            "pre": [
                "_l_true_m2",
                "_l_true_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_true_m2__l_true_p2"
        },
        {
            "pre": [
                "_l_true_m1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_true_m1__l_true_m1"
        },
        {
            "pre": [
                "_l_true_m1",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_true_m1__l_true_0"
        },
        {
            "pre": [
                "_l_true_m1",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_true_m1__l_true_p1"
        },
        {
            "pre": [
                "_l_true_m1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_true_m1__l_true_p2"
        },
        {
            "pre": [
                "_l_true_0",
                "_l_true_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_true_0__l_true_0"
        },
        {
            "pre": [
                "_l_true_0",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_true_0__l_true_p1"
        },
        {
            "pre": [
                "_l_true_0",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_true_0__l_true_p2"
        },
        {
            "pre": [
                "_l_true_p1",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_true_p1__l_true_p1"
        },
        {
            "pre": [
                "_l_true_p1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__l_true_p1__l_true_p2"
        },
        {
            "pre": [
                "_l_true_p2",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__l_true_p2__l_true_p2"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__l_false_m2__l_true_m2"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__l_false_m2__l_true_m1"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_true_0"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_true_p1"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_true_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_true_p2"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__l_false_m2__l_false_m2"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__l_false_m2__l_false_m1"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_false_0"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_false_p1"
        },
        {
            "pre": [
                "_l_false_m2",
                "_l_false_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_m2__l_false_p2"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__l_false_m1__l_true_m2"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_true_m1"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_true_0"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_true_p1"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_m1__l_true_p2"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_false_m1"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_false_0"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_m1__l_false_p1"
        },
        {
            "pre": [
                "_l_false_m1",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_m1__l_false_p2"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__l_false_0__l_true_m2"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_0__l_true_m1"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_true_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_0__l_true_0"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_0__l_true_p1"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_false_0__l_true_p2"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_false_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_0__l_false_0"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_0__l_false_p1"
        },
        {
            "pre": [
                "_l_false_0",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_false_0__l_false_p2"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__l_false_p1__l_true_m2"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_p1__l_true_m1"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_p1__l_true_0"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_false_p1__l_true_p1"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__l_false_p1__l_true_p2"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_false_p1__l_false_p1"
        },
        {
            "pre": [
                "_l_false_p1",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__l_false_p1__l_false_p2"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__l_false_p2__l_true_m2"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_true_m1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__l_false_p2__l_true_m1"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__l_false_p2__l_true_0"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__l_false_p2__l_true_p1"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__l_false_p2__l_true_p2"
        },
        {
            "pre": [
                "_l_false_p2",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__l_false_p2__l_false_p2"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_true_m2__l_true_m1"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_true_0"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_true_p1"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_true_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_true_p2"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__nl_true_m2__l_false_m2"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_true_m2__l_false_m1"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_false_0"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_false_p1"
        },
        {
            "pre": [
                "_nl_true_m2",
                "_l_false_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m2__l_false_p2"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_true_m1"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_true_0"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_true_p1"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_m1__l_true_p2"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_true_m1__l_false_m2"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_false_m1"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_false_0"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_m1__l_false_p1"
        },
        {
            "pre": [
                "_nl_true_m1",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_m1__l_false_p2"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_0__l_true_p1"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_0__l_true_p2"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_true_0__l_false_m2"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_0__l_false_m1"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_false_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_0__l_false_0"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_0__l_false_p1"
        },
        {
            "pre": [
                "_nl_true_0",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_0__l_false_p2"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p1__l_true_m2"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p1__l_true_m1"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p1__l_true_0"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p1__l_true_p1"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_true_p1__l_true_p2"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p1__l_false_m2"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p1__l_false_m1"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_false_0"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p1__l_false_0"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p1__l_false_p1"
        },
        {
            "pre": [
                "_nl_true_p1",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_true_p1__l_false_p2"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p2__l_true_m2"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_true_m1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p2__l_true_m1"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p2__l_true_0"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_true_p2__l_true_p1"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__nl_true_p2__l_true_p2"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_true_p2__l_false_m2"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_false_m1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p2__l_false_m1"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_false_0"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_true_p2__l_false_0"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_true_p2__l_false_p1"
        },
        {
            "pre": [
                "_nl_true_p2",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__nl_true_p2__l_false_p2"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__nl_false_m2__l_true_m2"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_false_m2__l_true_m1"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_true_0"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_true_p1"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_true_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_true_p2"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m2"
            ],
            "name": "x__nl_false_m2__l_false_m2"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_false_m2__l_false_m1"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_false_0"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_false_p1"
        },
        {
            "pre": [
                "_nl_false_m2",
                "_l_false_p2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m2__l_false_p2"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_false_m1__l_true_m2"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_true_m1"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_true_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_true_0"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_true_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_true_p1"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_m1__l_true_p2"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_m1"
            ],
            "name": "x__nl_false_m1__l_false_m2"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_false_m1"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_false_0"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_false_0"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_false_p1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_m1__l_false_p1"
        },
        {
            "pre": [
                "_nl_false_m1",
                "_l_false_p2"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_m1__l_false_p2"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_true_m2"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_true_m1"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_true_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_true_0"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_0__l_true_p1"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_false_0__l_true_p2"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m2",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_false_m2"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_false_m1"
        },
        {
            "pre": [
                "_nl_false_0",
                "_l_false_0"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_0__l_false_0"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p1__l_true_m2"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_true_m1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p1__l_true_m1"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p1__l_true_0"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p1__l_true_p1"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_false_p1__l_true_p2"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_m1",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p1__l_false_m2"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_false_m1"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p1__l_false_m1"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_false_0"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p1__l_false_0"
        },
        {
            "pre": [
                "_nl_false_p1",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p1__l_false_p1"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_true_m2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p2__l_true_m2"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_true_m1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p2__l_true_m1"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_true_0"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p2__l_true_0"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_true_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_false_p2__l_true_p1"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_true_p2"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p2"
            ],
            "name": "x__nl_false_p2__l_true_p2"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_false_m2"
            ],
            "post": [
                "_l_true_0",
                "_nl_true_0"
            ],
            "name": "x__nl_false_p2__l_false_m2"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_false_m1"
            ],
            "post": [
                "_l_false_p1",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p2__l_false_m1"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_false_0"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_0"
            ],
            "name": "x__nl_false_p2__l_false_0"
        },
        {
            "pre": [
                "_nl_false_p2",
                "_l_false_p1"
            ],
            "post": [
                "_l_false_p2",
                "_nl_false_p1"
            ],
            "name": "x__nl_false_p2__l_false_p1"
        }
    ],
    "states": [
        "_l_true_m2",
        "_l_true_m1",
        "_l_true_0",
        "_l_true_p1",
        "_l_true_p2",
        "_l_false_m2",
        "_l_false_m1",
        "_l_false_0",
        "_l_false_p1",
        "_l_false_p2",
        "_nl_true_m2",
        "_nl_true_m1",
        "_nl_true_0",
        "_nl_true_p1",
        "_nl_true_p2",
        "_nl_false_m2",
        "_nl_false_m1",
        "_nl_false_0",
        "_nl_false_p1",
        "_nl_false_p2"
    ],
    "initialStates": [
        "_l_true_m2",
        "_l_true_m1",
        "_l_true_0",
        "_l_true_p1",
        "_l_true_p2",
        "_l_false_m2",
        "_l_false_m1",
        "_l_false_0",
        "_l_false_p1",
        "_l_false_p2"
    ],
    "predicate": "-2 * _l_true_m2 + -1 * _l_true_m1 + 0 * _l_true_0 + 1 * _l_true_p1 + 2 * _l_true_p2 + -2 * _l_false_m2 + -1 * _l_false_m1 + 0 * _l_false_0 + 1 * _l_false_p1 + 2 * _l_false_p2 < 1",
    "trueStates": [
        "_l_true_m2",
        "_l_true_m1",
        "_l_true_0",
        "_l_true_p1",
        "_l_true_p2",
        "_nl_true_m2",
        "_nl_true_m1",
        "_nl_true_0",
        "_nl_true_p1",
        "_nl_true_p2"
    ],
    "title": "Old Threshold Protocol with l = 2 and c = 1"
}