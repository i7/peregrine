{-# LANGUAGE OverloadedStrings #-}

module Printer.DOT
    (printPopulationProtocol)
where

import qualified Data.ByteString.Lazy as L
import Data.ByteString.Builder
import Data.Monoid
import Data.List (elem)

import Printer
import PopulationProtocol

renderPopulationProtocol :: PopulationProtocol -> Builder
renderPopulationProtocol pp =
            "digraph populationprotocol {\n" <>
            mconcat (map stateLabel (states pp)) <>
            mconcat (map transLabel (transitions pp)) <>
            "}\n"
        where
            stateLabel q = renderState q <>  " [label=\"" <> renderState q <>
                (if q `elem` initialStates pp then " (i)" else "") <> "\"];\n"
            transLabel t = renderTransition t <>
                " [label=\"" <> renderTransition t <> "\", shape=box, " <>
                "style=filled, fillcolor=\"#AAAAAA\"];\n" <>
                mconcat (map (\q -> arcLabel (q,t)) (pre pp t)) <>
                mconcat (map (\q -> arcLabel (t,q)) (post pp t))
            arcLabel (a,b) = renderShow a <> " -> " <> renderShow b <> "\n"

printPopulationProtocol :: PopulationProtocol -> L.ByteString
printPopulationProtocol = toLazyByteString . renderPopulationProtocol
