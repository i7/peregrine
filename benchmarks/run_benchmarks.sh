#!/bin/bash

protocols_dir='protocols'
out_dir='out'

extension='pp'
executable_dir='../dist/build/peregrine'
executable='peregrine'
options='-i -v'

#1 hour
timelimit=$((1 * 3600))

results_file="$1"
properties="${@:2}"

declare -A prop_options
prop_options['termination']='--layered-termination'
prop_options['consensus']='--strong-consensus'
prop_options['correctness']='--correctness'

declare -A prop_refinements
prop_refinements['termination']='Checking SAT of layered termination'
prop_refinements['consensus']='Checking SAT of trap'
prop_refinements['correctness']='Checking SAT of trap'

structure_option='--structure'

mkdir -p $out_dir

>$results_file
echo -n "protocol,file,states,transitions" >>$results_file
for prop in $properties; do
    echo -n ",${prop}_result,${prop}_time,${prop}_refinements" >>$results_file
done

echo >>$results_file

for protocol_dir in $(find $protocols_dir -mindepth 1 -maxdepth 1 -type d); do
    protocol=$(basename $protocol_dir)
    echo
    echo "Benchmarking $protocol protocol"
    echo

    mkdir -p $out_dir/$protocol
    timeout_reached='false'
    for param in $(seq 1 1000); do
        if [ $timeout_reached == 'false' ]; then
            for filename in $(find $protocol_dir -mindepth 1 -maxdepth 1 -name "${protocol}_?${param}_*.$extension"); do
                file=$(basename $filename)
                echo -n "$protocol,$file" >>$results_file

                (
                    $executable_dir/$executable $options $structure_option $filename 2>&1 | tee $out_dir/$protocol/$file.out
                )
                n_states=$(grep -e '^States *: ' "$out_dir/$protocol/$file.out" | sed -e 's/^.*: \([0-9]*\)$/\1/')
                n_transitions=$(grep -e '^Transitions *: ' "$out_dir/$protocol/$file.out" | sed -e 's/^.*: \([0-9]*\)$/\1/')
                echo -n ",$n_states,$n_transitions" >>$results_file

                for prop in $properties; do
                    prop_option=${prop_options[$prop]}
                    prop_refinement=${prop_refinements[$prop]}
                    echo "Benchmarking $file with property $prop"
                    timing="$(date +%s%N)"
                    (
                        set -o pipefail

                        echo timeout $timelimit $executable_dir/$executable $options $prop_option $filename
                        timeout $timelimit $executable_dir/$executable $options $prop_option $filename 2>&1 | tee $out_dir/$protocol/$file.$prop.out
                    )
                    result=$?
                    timing=$(($(date +%s%N)-timing))
                    if [[ result -eq 0 ]]; then
                        res='positive'
                    elif [[ result -eq 2 ]]; then
                        res='dontknow'
                    elif [[ result -eq 124 || result -eq 137 ]]; then
                        res='timeout'
                    else
                        res='error'
                    fi
                    # get number of refinement steps
                    n_refinements=$(grep -c -e "$prop_refinement" "$out_dir/$protocol/$file.$prop.out")
                    echo -n ",$res,$timing,$n_refinements" >>$results_file
                    if [ $res == 'timeout' ]; then
                        timeout_reached='true'
                    fi
                done

                echo >>$results_file
            done
        fi
    done
done
