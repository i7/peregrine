{-# LANGUAGE DeriveGeneric #-}

module ProtocolOutput where

import Util
import qualified PopulationProtocol as P
import Data.Aeson
import Data.Aeson.Encode.Pretty 
import qualified Data.ByteString.Lazy as BS
import GHC.Generics (Generic)

nonSilentActions :: (Ord qs) => P.PopulationProtocol qs -> [(qs, qs)]
nonSilentActions pp = [(q1, q2) | q1 <- P.states pp
                                , q2 <- P.states pp
                                , q1 <= q2
                                , (P.trans pp) (q1, q2) /= (q1, q2)
                                , (P.trans pp) (q1, q2) /= (q2, q1)]


places :: (Show qs) => qs -> qs -> String
places q1 q2 =  "{ " ++ toVar q1 ++ ", " ++ toVar q2 ++ " }"

data TransStruct = TransStruct { name :: String
                               , pre :: [String]
                               , post :: [String]
                               } deriving (Eq, Ord, Show, Generic)

data PPStruct = PPStruct { title :: String
                         , states :: [String]
                         , transitions :: [TransStruct]
                         , initialStates :: [String]
                         , trueStates :: [String]
                         , predicate :: Maybe String
                         } deriving (Eq, Ord, Show, Generic)

instance ToJSON TransStruct
instance ToJSON PPStruct
                                  

pp2JSON :: (Show qs, Ord qs) => P.PopulationProtocol qs -> String -> BS.ByteString
pp2JSON pp str = encodePretty $ 
        PPStruct { title = str
                 , states = toVar <$> P.states pp
                 , transitions = [ TransStruct { name = transToVar (q1, q2)
                                               , pre = toVar <$> [q1, q2]
                                               , post = toVar <$> [q1', q2']
                                               }
                                 | (q1, q2) <- nonSilentActions pp
                                 , let (q1', q2') = (P.trans pp) (q1, q2)
                                 ]
                 , initialStates = toVar <$> P.initial pp
                 , trueStates = toVar <$> [q | q <- P.states pp, (P.opinion pp) q]
                 , predicate = P.predicate pp
                 }
