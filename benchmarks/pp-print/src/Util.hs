module Util (transToVar, toVar) where

import Data.Char (toLower)

transToVar :: (Show qs) => (qs, qs) -> String
transToVar (q1, q2) = "x_" ++ toVar q1 ++ "_" ++ toVar q2

toVar :: (Show a) => a -> String
toVar = ('_':) . map toLower . filter (\x -> not (x  `elem` " ,()")) . show

