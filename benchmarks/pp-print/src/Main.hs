import System.Environment (getArgs)
import PopulationProtocol
import ProtocolOutput
import qualified Data.ByteString.Lazy as BS

usageAction :: IO ()
usageAction = do 
    putStrLn "Usage: "
    putStrLn "'./pp2petrinet effThresholdPP c' converts .. to PNet."
    putStrLn "'./pp2petrinet majorityPP' converts Majority PP to PNet."
    putStrLn "'./pp2petrinet broadCastPP' converts BroadCast PP to PNet."
    putStrLn "'./pp2petrinet thresholdPP l c' converts threshold PP to PNet, where l and c are positive integers."
    putStrLn "'./pp2petrinet moduloPP m c' converts Modulo PP to PNet, where m, c are positive integers."
    putStrLn "'./pp2petrinet oldThresholdPP l c' converts Modulo PP to PNet, where l, c are positive integers."
    putStrLn "'./pp2petrinet flockOfBirdsPP c' converts FlockOfBirds PP to PNet, where c is a positive integer."
    putStrLn "'./pp2petrinet newBirdsPP c' converts NewBirds PP to PNet, where c is a positive integer."
    putStrLn "'./pp2petrinet logFlockOfBirdsPP c' converts Polylog Flock-of-Birds PP to PNet, where c is a positive integer."
    putStrLn "'./pp2petrinet verifiableFlockOfBirdsPP c' converts VerifiableFlockOfBirds PP to PNet, where c is a positive integer."
    putStrLn "'./pp2petrinet fastMajorityPP m d' converts FastMajority PP to PNet, where m, d must be odd and positive integers."
    putStrLn "'./pp2petrinet layeredProtocol m' converts Protocol with m >= 1 termination layers to PNet."


majorityAction :: IO ()
majorityAction = BS.putStr $  pp2JSON createMajorityProtocol "Majority Protocol"

broadCastAction :: IO ()
broadCastAction = BS.putStr $  pp2JSON createBroadcastProtocol "BroadCast Protocol"

simpleAction :: IO ()
simpleAction = BS.putStr $  pp2JSON simpleProtocol "Simple Protocol"


newBirdsAction :: [String] -> IO ()
newBirdsAction [x] = let pp = createNewBirdsProtocol (read x) in
     BS.putStr $  pp2JSON  pp ("New birds protocol with c = " ++ x)


logFlockOfBirdsAction :: [String] -> IO ()
logFlockOfBirdsAction [x] = let pp = createLogFlockOfBirdsProtocol (read x) in
     BS.putStr $  pp2JSON  pp ("Log Flock of birds protocol with c = " ++ x)

effThresholdAction :: [String] -> IO () 
effThresholdAction [x] = let pp = createEfficientThresholdProtocol (read x) in
     BS.putStr $  pp2JSON  pp ("Efficient Flock of birds protocol with c = " ++ x)

flockOfBirdsAction :: [String] -> IO ()
flockOfBirdsAction [x] = let pp = createFlockOfBirdsProtocol (read x) in
     BS.putStr $  pp2JSON  pp ("Flock of birds protocol with c = " ++ x)

verifiableFlockOfBirdsAction :: [String] -> IO ()
verifiableFlockOfBirdsAction [x] = let pp = createVerifiableFlockOfBirdsProtocol (read x) in
     BS.putStr $  pp2JSON  pp ("Verifiable flock of birds protocol with c = " ++ x)

oldThresholdAction :: [String] -> IO ()
oldThresholdAction [x, y] = let  pp = createOldThresholdProtocol (read x) (read y) in
    BS.putStr $  pp2JSON  pp ("Old Threshold Protocol with l = " ++ x ++ " and c = " ++ y)
oldThresholdAction _ = usageAction


thresholdAction :: [String] -> IO ()
thresholdAction [x, y] = let (Just pp) = createThresholdProtocol (read x) (read y) in
    BS.putStr $  pp2JSON  pp ("Threshold Protocol with l = " ++ x ++ " and c = " ++ y)
thresholdAction _ = usageAction


moduloAction :: [String] -> IO ()
moduloAction [x, y] = let pp = createModuloProtocol (read x) (read y) in
    BS.putStr $  pp2JSON  pp ("Modulo Protocol with m = " ++ x ++ " and c = " ++ y)
moduloAction _ = usageAction


layeredProtocolAction :: [String] -> IO ()
layeredProtocolAction [m] = let pp = createMLayerProtocol (read m) in
    BS.putStr $  pp2JSON pp ("Layered Protocol with m = " ++ m ++ " layers.")

main = do
    args <- getArgs
    case args of
        ("majorityPP":_) -> majorityAction
        ("broadCastPP":_) -> broadCastAction
        ("thresholdPP":as) -> thresholdAction as
        ("oldThresholdPP":as) -> oldThresholdAction as
        ("moduloPP":as) -> moduloAction as
        ("flockOfBirdsPP":as) -> flockOfBirdsAction as
        ("effThresholdPP":as) -> effThresholdAction as
        ("newBirdsPP":as) -> newBirdsAction as
        ("verifiableFlockOfBirdsPP":as) -> verifiableFlockOfBirdsAction as
        ("logFlockOfBirdsPP":as) -> logFlockOfBirdsAction as
        ("simplePP":_) -> simpleAction
        ("layeredProtocol":as) -> layeredProtocolAction as
        _ -> usageAction 
