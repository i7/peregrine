import sys
import pandas as pd

prop_steps = {}
prop_steps['termination'] = '$|\mathcal{P}|$'
prop_steps['consensus']   = '$|R|$'
prop_steps['correctness'] = '$|R|$'

max_params = 2

long_table = True
short_table = False

results_file = 'results/results.csv'

if len(sys.argv) >= 2:
    for arg in sys.argv[1:]:
        if arg[0] == '-':
            if arg == '-s':
                long_table = False
                short_table = True
            elif arg == '-l':
                long_table = True
                short_table = False
            else:
                print("unknown argument: %s" % arg, file=sys.stderr)
                sys.exit(-1)
        else:
            results_file = arg

# long table

if long_table:

    results = pd.read_csv(results_file)

    properties = list(map(lambda s: s[:-7], filter(lambda s: s[-7:] == "_result", results.columns.values, )))

    alignrow = "\\begin{longtable}{l"
    for i in range(max_params):
        alignrow += 'l'
    alignrow += 'rr'
    for prop in properties:
        alignrow += 'rr'
    alignrow += 'r}'
    print(alignrow)
    print("\\toprule")

    headerrow = "protocol & \\multicolumn{%d}{c}{params} & $|Q|$ & $|T|$" % max_params
    for prop in properties:
        headerrow +=  " & %s & %s" % (prop, prop_steps[prop])
    headerrow += " & total \\\\"
    print(headerrow)

    last_protocol = ''
    last_params = []
    for i in range(max_params):
        last_params.append('')

    for result in results.iterrows():
        row = result[1]
        row_string = ''

        protocol = row['protocol']
        filename = row['file']

        protocol_changed = False
        if protocol != last_protocol:
            protocol_changed = True
            print("\\midrule")
            row_string = "%s" % protocol
            last_protocol = protocol
            for i in range(max_params):
                last_params[i] = ''
        else:
            row_string = ""

        params = [ p for p in filename[len(protocol):].split('.')[0].split('_') if len(p) >= 2 ]

        for i in range(len(params)):
            var = params[i][0]
            val = params[i][1:]
            param_string = "$%s = %s$" % (var, val)
            if last_params[i] != param_string:
                #if i == 0 and len(params) > 1 and not protocol_changed:
                #    print("\\cline{2-%d}" % (4 + max_params + 2*len(properties)))
                last_params[i] = param_string
                row_string += " & %s" % param_string
                for j in range(i+1, max_params):
                    last_params[j] = ''
            else:
                row_string += " &"
        for i in range(len(params), max_params):
            row_string += " &"

        n_states = row['states']
        n_transitions = row['transitions']
        row_string += ' & %d & %d' % (n_states, n_transitions)

        total_time = 0.0
        total_timeout = False

        for prop in properties:
            prop_result = row[prop + '_result']
            prop_time = row[prop + '_time'] / 10**9
            prop_refinements = row[prop + '_refinements']
            prop_string = ''
            if prop_result == 'positive':
                prop_string = "%.2f" % prop_time
            elif prop_result == 'dontknow':
                prop_string = "%.2f (?)" % prop_time
            elif prop_result == 'timeout':
                prop_string = 'timeout'
                total_timeout = True
            else:
                prop_string = 'error'
            try:
                refinement_string = str(int(prop_refinements))
            except ValueError:
                refinement_string ='-'
            row_string += " & %s & %s" % (prop_string, refinement_string)
            total_time += prop_time

        row_string += " & "
        if total_timeout:
            row_string += "timeout"
        else:
            row_string += "%.2f" % total_time
        row_string += " \\\\"

        print(row_string)

    print("\\bottomrule")
    print("\\end{longtable}")

# short table

if short_table:

    results = pd.read_csv(results_file)

    properties = list(map(lambda s: s[:-7], filter(lambda s: s[-7:] == "_result", results.columns.values, )))

    last_protocol = ''
    last_params = []
    for i in range(max_params):
        last_params.append('')

    for result in results.iterrows():
        row = result[1]
        row_string = ''

        protocol = row['protocol']
        filename = row['file']

        protocol_changed = False
        if protocol != last_protocol:
            protocol_changed = True
            if last_protocol != '':
                print("\\bottomrule")
                print("\\end{tabular}")
            last_protocol = protocol
            print("\\begin{tabular}{rrrr}")
            print("\\toprule")
            print("\multicolumn{3}{c}{%s} \\\\" % protocol)
            print("\\midrule")
            print("$m$ & $|Q|$ & $|T|$ & Time\,(s) \\\\")
            print("\\midrule")

        params = [ p for p in filename[len(protocol):].split('.')[0].split('_') if len(p) >= 2 ]

        if len(params) > 0:
            val = params[0][1:]
            row_string += "%s " % val

        n_states = row['states']
        n_transitions = row['transitions']
        row_string += ' & %d & %d' % (n_states, n_transitions)

        total_time = 0.0
        total_timeout = False
        total_error = False

        for prop in properties:
            prop_result = row[prop + '_result']
            if prop_result == 'positive':
                prop_time = row[prop + '_time'] / 10**9
                total_time += prop_time
            elif prop_result == 'timeout':
                total_timeout = True
            else:
                total_error = True

        row_string += " & "
        if total_timeout:
            row_string += "timeout"
        elif total_error:
            row_string += "error"
        else:
            row_string += "%.1f" % total_time
        row_string += " \\\\"

        print(row_string)

    if last_protocol != '':
        print("\\bottomrule")
        print("\\end{tabular}")

