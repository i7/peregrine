# EDIT_WYSIWYG_ONLY
# -*- coding: utf-8 -*-
# This file has been created by Peregrine.
# Do not edit manually!
def generateProtocol(params):
    return {
      "title":         "Broadcast protocol",
      "states":        ["yes", "no"],
      "transitions":   [Utils.transition(["yes", "no"], ["yes", "yes"])],
      "initialStates": ["yes", "no"],
      "trueStates":    ["yes"],
      "predicate":     "C[yes] >= 1",

      "description":   """This protocol computes whether at least one agent is
                          initially in state yes."""
    }