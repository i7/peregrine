#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import json
import re

class Utils:
    @staticmethod
    def transition(pre, post):
        return {
            "name": "{}, {} -> {}, {}".format(pre[0], pre[1], post[0], post[1]),
            "pre":  [pre[0],  pre[1]],
            "post": [post[0], post[1]]
        }

    @staticmethod
    def silent(pre, post):
        return ((pre[0] == post[0] and pre[1] == post[1]) or
                (pre[0] == post[1] and pre[1] == post[0]))

n = len(sys.argv)

def to_string(p):
    return "s" + re.sub(r'[^a-zA-Z0-9_]', r'_', str(p))

if (n != 2 and n != 3):
    print("Usage: program [file path] [param dict]")
else:
    script = sys.argv[1]
    exec(open(script).read())

    if (n == 3):
        params = json.loads(sys.argv[2])
        protocol = generateProtocol(params)

        # Convert states to strings
        for p in ["states", "initialStates", "trueStates"]:
            protocol[p] = list(map(to_string, protocol[p]))

        for i in range(len(protocol["transitions"])):
          for p in ["pre", "post"]:
            protocol["transitions"][i][p] = list(map(to_string, protocol["transitions"][i][p]))

        protocol["predicate"] = re.sub(r'C\[([^]]*)\]', lambda match: to_string(match.group(1)), protocol["predicate"])
        if "precondition" in protocol:
          protocol["precondition"] = re.sub(r'C\[([^]]*)\]', lambda match: to_string(match.group(1)), protocol["precondition"])

        if "statesStyle" in protocol:
          style = protocol["statesStyle"]
          protocol["statesStyle"] = {str(p): style[p] for p in style}

        print(json.dumps(protocol))
    else:
        try:
            params
        except NameError:
            params = {}

        print(json.dumps(params))
