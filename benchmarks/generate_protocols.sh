#!/bin/bash

benchmark_dir='.'
executable='./pp-print/dist/build/pp-print/pp-print'
protocols_dir='protocols'

mkdir -p $benchmark_dir/$protocols_dir

# majority
echo "generating majority protocol"
mkdir -p $benchmark_dir/$protocols_dir/majority
$executable majorityPP >$benchmark_dir/$protocols_dir/majority/majority_m1_.pp

# fast majority
echo "generating fast majority protocol"
mkdir -p $benchmark_dir/$protocols_dir/fastmajority
cp ../examples/fastmajority.pp $benchmark_dir/$protocols_dir/fastmajority/fastmajority_m1_.pp

# broadcast
echo "generating broadcast protocol"
mkdir -p $benchmark_dir/$protocols_dir/broadcast
$executable broadCastPP >$benchmark_dir/$protocols_dir/broadcast/broadcast_m1_.pp

# threshold
echo "generating threshold protocols"
mkdir -p $benchmark_dir/$protocols_dir/threshold
for l in 2 3 4 5 6 7 8 9 10 11 12 13 14 15; do
    for c in 1; do
        $executable oldThresholdPP $l $c >$benchmark_dir/$protocols_dir/threshold/threshold_l${l}_c${c}_.pp
    done
done

# Remainder
echo "generating remainder protocols"
mkdir -p $benchmark_dir/$protocols_dir/remainder
for m in 2 3 4 5 6 7 8 9 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 90 100 110 120 130 140 150; do
    for c in 1; do
        $executable moduloPP $m $c >$benchmark_dir/$protocols_dir/remainder/remainder_m${m}_c${c}_.pp
    done
done

# Flock of Birds from Chatzigianniks, Michail, Spirakis: Algorithmic verification of population protocols
echo "generating flockofbirds protocols"
mkdir -p $benchmark_dir/$protocols_dir/flockofbirds
for c in 10 15 20 25 30 35 40 45 50 55 60 65 70 80 90 100 110 120 130 140 150; do
    $executable flockOfBirdsPP $c >$benchmark_dir/$protocols_dir/flockofbirds/flockofbirds_c${c}_.pp
done

# new birds from Clément, Delporte-Gallet, Fauconnier, Sighireanu: Guidelines for the verification of population protocols
echo "generating newbirds protocols"
mkdir -p $benchmark_dir/$protocols_dir/newbirds
for c in 50 100 150 200 250 300 350 400 450 500 550 600 650 700 750 800; do
    $executable newBirdsPP $c >$benchmark_dir/$protocols_dir/newbirds/newbirds_c${c}_.pp
done

# Logarithmic Flock of Birds
echo "generating logarithmic flock of birds protocols"
mkdir -p $benchmark_dir/$protocols_dir/logflockofbirds
for clog in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 25 30 35 40; do
    c=$(python -c "print(10**${clog})")
    python python/execprotocol.py python/flock_log.py "{\"scheme\": { \"threshold\": { \"value\": ${c} } } }" >$benchmark_dir/$protocols_dir/logflockofbirds/logflockofbirds_c${clog}_.pp
done

# Tower Flock of Birds
echo "generating tower flock of birds protocols"
mkdir -p $benchmark_dir/$protocols_dir/towerflockofbirds
for c in 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400 425 450 475 500 525 550 575 600; do
    python python/execprotocol.py python/flock_of_birds_-_tower.py "{\"scheme\": { \"c\": { \"value\": ${c} } } }" >$benchmark_dir/$protocols_dir/towerflockofbirds/towerflockofbirds_c${c}_.pp
done

# Average and Conquer
echo "generating average and conquer protocol"
mkdir -p $benchmark_dir/$protocols_dir/avc
n_lower=2
#for m in 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31 33 35 37 39 41 45 51 55 61 65 71 75 81 85 91 95 101; do
for m in 15 25 35 45 55 65 75 85 95 105; do
    n_upper=10000000
##     while [[ $(($n_upper - $n_lower)) -gt 1 ]]; do
##         n=$(((n_upper + n_lower) / 2))
##         mlow=$(python -c "import math; print(math.ceil(math.log(${n})*math.log(math.log(${n}))))")
##         #echo "At m=$m, intervl [$n_lower, $n_upper], n = $n; m'=$mlow"
##         if [[ $mlow -le $m ]]; then
##             n_lower=$n
##         else
##             n_upper=$n
##         fi
##     done
##     n=$n_lower
##     d=$(python -c "import math; print(math.ceil(math.log(${m})*math.log(${n})))")
##     echo "For m=$m got n=$n, d=$d"
    d=1
    python python/execprotocol.py python/avc.py "{\"scheme\": { \"m\": { \"value\": ${m} }, \"d\": { \"value\": ${d} } } }">$benchmark_dir/$protocols_dir/avc/avc_m${m}_d${d}_.pp
done
